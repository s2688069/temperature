package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestConverter {
    @Test
    public void testTemperature() throws Exception {
        Converter temperature = new Converter();
        double price = temperature.getTemperature("0");
        Assertions.assertEquals(32, price, 0.0, "Temperature in Fahrenheit of 0C");
    }
}