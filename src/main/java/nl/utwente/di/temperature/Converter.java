package nl.utwente.di.temperature;

public class Converter {

    public double getTemperature(String temperature) {
        Double celsius = Double.parseDouble(temperature);
        return (celsius * 1.8) + 32;
    }

}
